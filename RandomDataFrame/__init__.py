from pandas import DataFrame as PDDF
from numpy import dtype
from functools import cache

from typing import Optional, Collection, Dict, Callable, Type, AnyStr
from random import randint, random, choice


class ColumnDef:
    """
    dtype details: https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.astype.html
    """

    def __init__(self, name: str, func: Callable):
        self.name: str = name
        self.func: Callable = func

    def __call__(self, *args, **kwargs):
        return self.func(*args, **kwargs)


class RandomDataFrame:
    Default_column_def = [ColumnDef('index', lambda ri: ri),
                          ColumnDef('rint', lambda ri: randint(a=4, b=33)),
                          ColumnDef('rfloat', lambda ri: random()),
                          ColumnDef('rstr', lambda ri: choice(['this', 'that', 'those', 'these']))
                          ]

    @classmethod
    def Simple_Table(cls, caching: bool = True):
        return cls(column_def=cls.Default_column_def,
                   caching=caching)

    def __init__(self,
                 column_def: Collection[ColumnDef],
                 caching: bool = True,
                 axis: int = 0):
        """
        column_def: The column definition for each column in the dataframe
        caching: if caching == True then calling 'dataframe' instance method will pull from internal cache of
        dataframes to return a previously generated one if it exists.  Otherwise if caching == False then new
        dataframes will always be generated.
        axis definition from: https://stackoverflow.com/a/22149930/3395539
        0 = random data is generated row by row till row_count
        1 = random data is generated colum by column till each column count satisfies row_count
        """
        self.column_def = column_def
        self.caching = caching
        self.axis = axis

    @property
    def column_name_list(self):
        return [x.name for x in self.column_def]

    def dataframe(self, row_count: int = 10, **kwargs):
        return self._dataframe_cached(row_count=row_count, **kwargs) if self.caching else self._dataframe(row_count=row_count, **kwargs)

    @cache
    def _dataframe_cached(self, row_count: int = 10, **kwargs) -> PDDF:
        return self.build_dataframe(row_count=row_count, **kwargs)

    def _dataframe(self, row_count: int = 10, **kwargs) -> PDDF:
        return self.build_dataframe(row_count=row_count, **kwargs)


    def build_column(self, column_index: int, row_count: int) -> Collection:
        """
        Build one specific column containing all the row values for that column.
        """
        return [self.column_def[column_index](row_index) for row_index in range(row_count)]

    def build_row(self, row_index: int) -> Collection:
        """
        Build one row that is composed of a tuple 1 value for each column.
        """
        return tuple(self.column_def[column_index](row_index) for column_index in range(len(self.column_def)))

    def build_dataframe(self, row_count: int, **kwargs):
        if self.axis == 1:
            return self.build_dataframe_column_wise(row_count=row_count, **kwargs)
        elif self.axis == 0:
            return self.build_dataframe_row_wise(row_count=row_count, **kwargs)
        else:
            raise ValueError("axis can only be 0 or 1.")

    def build_dataframe_row_wise(self, row_count: int, **kwargs):
        """
        self.axis == 0
        given column definition, generate a random dataframe.
        build data frame by building each row in the data frame...
        """
        return PDDF(data=[self.build_row(row_index) for row_index in range(row_count)],
                    columns=self.column_name_list,
                    **kwargs
                    )

    def build_dataframe_column_wise(self, row_count: int, **kwargs):
        """
        self.axis == 1
        given column definition, generate a random dataframe.
        build data frame by building each column in the data frame...
        I would rather do row wise building... but this will do for now.
        """
        return PDDF(
            data={self.column_def[i].name: self.build_column(column_index=i, row_count=row_count) for i in range(len(
                self.column_def))},
            columns=self.column_name_list,
            **kwargs)
