import pandas as pd

from . import RandomDataFrame
import pytest

"""
I do a lot of code reviews.  I have found that some developers tend to move from
dataframe back to something that they are familiar with when faced with a data access challenge.
One example of this is accessing a specific cell from a specific row in a dataframe.
ONE way to do this is convert the data frame to a list of dictionaries and then 
access the cell using dictionary key name.
An alternative way to do this is to use the dataframe iloc property to get the 
row in the dataframe then use either dictionary or property accessor to retrieve the item.
This Benchmark compares these two approaches to see which is faster in practice.
"""

@pytest.fixture()
def rdf():
    yield RandomDataFrame.Simple_Table(caching=False)

def build_dataframe_with_test_row(rdf: RandomDataFrame, row_count):
    special_top_row = pd.DataFrame(data=[rdf.build_row(row_index=0)],
                                   columns=rdf.column_name_list)
    special_top_row.iat[0, 3] = 'testing'
    return pd.concat([special_top_row, rdf.dataframe(row_count=row_count)], axis=0)

def access_row_using_to_dict(df):
    return df.to_dict(orient='records')[0]['rstr']

def access_row_using_iloc(df):
    return df.iloc[0].rstr

@pytest.mark.parametrize('row_count',[2])
def test_to_dict_index(rdf, row_count):
    expected_value = 'testing'
    df = build_dataframe_with_test_row(rdf, row_count)
    assert access_row_using_to_dict(df) == expected_value

@pytest.mark.parametrize('row_count',[2,200,200000])
def test_benchmark_to_dict_index(rdf, row_count, benchmark):
    df = build_dataframe_with_test_row(rdf, row_count)
    benchmark(access_row_using_to_dict, df)


@pytest.mark.parametrize('row_count',[2])
def test_iloc_index(rdf, row_count):
    expected_value = 'testing'
    df = build_dataframe_with_test_row(rdf, row_count)
    assert access_row_using_iloc(df) == expected_value

@pytest.mark.parametrize('row_count',[2,200,200000])
def test_benchmark_iloc_index(rdf, row_count, benchmark):
    df = build_dataframe_with_test_row(rdf, row_count)
    benchmark(access_row_using_iloc, df)

def test_demo_dataframe(rdf):
    df = build_dataframe_with_test_row(rdf, 2)
    df.to_markdown('demo_dataframe.md')

