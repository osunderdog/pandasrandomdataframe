import pytest
from random import randint, random, choice
from RandomDataFrame import RandomDataFrame, ColumnDef, cache
from pandas import DataFrame

TEST_STR_LIST = ['this', 'that', 'those', 'these']

def column_calculation(row_index: int):
    return row_index+4

TEST_COLUMN_DEF = [ColumnDef('index', lambda ri: ri),
                   ColumnDef('rint', lambda ri: randint(a=4, b=33)),
                   ColumnDef('rfloat', lambda ri: random()),
                   ColumnDef('rfunc', column_calculation),
                   ColumnDef('rstr', lambda ri: choice(TEST_STR_LIST))
                   ]

@pytest.fixture
def rdf_with_cache():
    return RandomDataFrame(column_def=TEST_COLUMN_DEF, caching=True)

@pytest.fixture
def rdf():
    return RandomDataFrame(column_def=TEST_COLUMN_DEF, caching=False)


def test_basic():
    expected_row_count = 10
    # axis == 0 ROW Wise Creation
    gdf = RandomDataFrame(column_def=TEST_COLUMN_DEF,
                          axis=0)
    df = gdf.build_dataframe(row_count=expected_row_count)
    assert expected_row_count == len(df)
    assert len(df.columns) == len(TEST_COLUMN_DEF)

    # axis == 1 COLUMN Wise Creation
    gdf = RandomDataFrame(column_def=TEST_COLUMN_DEF,
                          axis=1)
    df = gdf.build_dataframe(row_count=expected_row_count)
    assert expected_row_count == len(df)
    assert len(df.columns) == len(TEST_COLUMN_DEF)

def test_row_count_option(rdf_with_cache):
    gdf = rdf_with_cache
    expected_row_count = 10
    mydf = gdf.dataframe(row_count=expected_row_count)
    assert expected_row_count == len(mydf)

    expected_row_count = 100
    mydf = gdf.dataframe(row_count=expected_row_count)
    assert expected_row_count == len(mydf)

def test_df_result_copy(rdf, rdf_with_cache):
    """Oh yeah, if you retrieve a dataframe, then modify the dataframe, you should probably copy it.  Otherwise
    you'll affect the cached version of the dataframe"""

    # Example where dataframe unique and changes to first access will not influence second access
    df1: DataFrame = rdf.dataframe(10)
    df1['baz'] = 1
    df2: DataFrame = rdf.dataframe(10)
    assert 'baz' not in df2.columns

    # the change to one dataframe will be reflected in subsequent dataframes
    df1: DataFrame = rdf_with_cache.dataframe(10)
    df1['baz'] = 1

    df2: DataFrame = rdf_with_cache.dataframe(10)
    assert 'baz' in df2.columns


@pytest.mark.parametrize('expected_row_count', [10,1000,100000])
def test_benchmark_df_by_row(expected_row_count, benchmark, rdf_with_cache):
    """
    Test lower level function timing. Is by row or by column faster?
    """
    benchmark(rdf_with_cache.build_dataframe_row_wise, row_count=expected_row_count)

@pytest.mark.parametrize('expected_row_count', [10,1000,100000])
def test_benchmark_df_by_column(expected_row_count, benchmark, rdf_with_cache):
    """
    Test lower level function timing. Is by row or by column faster?
    """
    benchmark(rdf_with_cache.build_dataframe_column_wise, row_count=expected_row_count)

@pytest.mark.parametrize('expected_row_count', [10,1000,100000])
def test_benchmark_cache_pedantic(expected_row_count, benchmark, rdf_with_cache):
    """
    Test cache table retrieval.  Benchmark should be fast because cache is primed by the warmup_round
    """
    benchmark.pedantic(rdf_with_cache.dataframe, kwargs={'row_count': expected_row_count},
                       iterations=20, rounds=150000, warmup_rounds=5)


def test_cache_statistics_check(rdf_with_cache):
    """
    Check the cache stats to make sure that they are reflecting reality
    """
    rdf_with_cache._dataframe_cached.cache_clear()
    rdf_with_cache.dataframe(row_count=100)
    rdf_with_cache.dataframe(row_count=100)
    rdf_with_cache.dataframe(row_count=101)
    rdf_with_cache.dataframe(row_count=101)
    rdf_with_cache.dataframe(row_count=102)
    rdf_with_cache.dataframe(row_count=102)
    rdf_with_cache.dataframe(row_count=102)

    cache_info = rdf_with_cache._dataframe_cached.cache_info()
    assert cache_info.currsize == 3
    assert cache_info.misses == 3
    assert cache_info.hits == 4
